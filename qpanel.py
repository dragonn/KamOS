import serial
import time

from PyQt5 import QtCore

class QPanel(QtCore.QThread):
    #lag = 127
    #ldg = 127
    
    #rag = 127
    #rdg = 127
    
    #lad = 127
    #ldd = 127

    #rad = 127
    #rdd = 127

    _x0 = 127
    _y0 = 127

    _x1 = 127
    _y1 = 127

    _x2 = 127
    _y2 = 127

    _x3 = 127
    _y3 = 127

    bin = [False, False, False, False, False, False]
    bin_old = [False, False, False, False, False, False]

    #joystick 3
    bin0 = QtCore.pyqtSignal(bool)

    #joystick 1
    bin1 = QtCore.pyqtSignal(bool)

    #joystick 2
    bin2 = QtCore.pyqtSignal(bool)
    bin3 = QtCore.pyqtSignal(bool)
    bin4 = QtCore.pyqtSignal(bool)
    bin5 = QtCore.pyqtSignal(bool)

    def __init__(self, port, speed, calibration):
        QtCore.QThread.__init__(self)

        self.port=port
        self.speed=speed
        self.calibration=calibration
        self._exiting = False

    def __del__(self):
        '''
        We are leaving now ...
        '''
        self._exiting = True
        self.wait()

    def run(self):
        self.open()
        while not self._exiting:
            try:
                line=self.serial.readline()
                
                line=line.split(b",")
                if len(line) == 18:
                #LAG,132,LDG,127,RAG,126,RDG,116,LAD,115,LDD,255,RAD,153,RDD,119,B,000000
                #print(line)
                    #self.lag=int(line[1])
                    #self.ldg=int(line[3])

                    #self.rag=int(line[5])
                    #self.rdg=int(line[7])

                    #self.lad=int(line[9])
                    #self.ldd=int(line[11])

                    #self.rad=int(line[13])
                    #self.rdd=int(line[15])
                    
                    #rdd
                    self._x0=int(line[15])
                    #rag
                    self._y0=int(line[5])

                    #rdg
                    self._x1=int(line[7])
                    #lad
                    self._y1=int(line[9])

                    #ldd
                    self._x2=int(line[11])
                    #rad
                    self._y2=int(line[13])

                    #lag
                    self._x3=int(line[1])
                    #ldg
                    self._y3=int(line[3])

                    self.bin=[ b==49 for b in line[17].strip() ]

                    for index, value in enumerate(self.bin):
                        if value != self.bin_old[index]:
                            getattr(self, 'bin'+str(index)).emit(value)
                    
                    self.bin_old = self.bin
                        
                    
            except Exception as e:
                self.serial.close()
                self.open()
                print(line)
                print(e)

    def open(self):
        connected = False
        while not connected:
            try:
                self.serial = serial.Serial(self.port, self.speed)
                connected = True
            except Exception as e:
                print(e)
                print(self.port)
                print("open_panel error")
                time.sleep(1)

    def calibrate(self, id, value):
        if value > self.calibration[id]['half'] - self.calibration[id]['dead'] and value < self.calibration[id]['half'] + self.calibration[id]['dead']:
            return 127
        else:
            return value

    @property
    def x0(self):
        return self.calibrate('x0', self._x0)
    @property
    def y0(self):
        return self.calibrate('y0', self._y0)

    @property
    def x1(self):
        return self.calibrate('x1', self._x1)
    @property
    def y1(self):
        return self.calibrate('y1', self._y1)

    @property
    def x2(self):
        return self.calibrate('x2', self._x2)
    @property
    def y2(self):
        return self.calibrate('y2', self._y2)

    @property
    def x3(self):
        return self.calibrate('x3', self._x3)
    @property
    def y3(self):
        return self.calibrate('y3', self._y3)


    
        
    