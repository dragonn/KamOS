from PyQt5 import QtGui, QtCore, QtWidgets, QtOpenGL
from ui_console import Ui_Console

class Console_GUI(QtWidgets.QWidget):
    def __init__(self, radio):
        QtWidgets.QWidget.__init__(self, None)
        self.ui = Ui_Console()
        self.ui.setupUi(self)
        self.ui.sendButton.clicked.connect(self.sendButton)

        self.radio = radio
        self.radio.inputs255cmd0.connect(self.recvCommand)

    def sendButton(self):
        cmd = self.ui.sendCommand.text()
        self.ui.listCommand.addItem('>>>> '+cmd)
        try:
            cmd = [int(x) for x in bytearray.fromhex(cmd)]
            self.radio.put(0xFF, 0x00, cmd)
        except Exception:
            self.ui.listCommand.addItem('>>>> błędny pakiet')
            pass
        
        

    def recvCommand(self, values):
        values = values[:-2].hex()
        self.ui.listCommand.addItem('<<<< ' + values)
        
        
