import sys
import config
import math

from PyQt5 import QtGui, QtCore, QtWidgets, QtOpenGL

from qjoystick import QJoystick

if config.radio['driver']=='tcp':
    from qradio_tcp import QRadio
elif config.radio['driver']=='serial':
    from qradio_serial import QRadio

from qvideo import QVideo, QVideoFrame

from ui_main import Ui_Form
from pygame import joystick

#try:
#    from OpenGL import GL
#except ImportError:
#    app = QtWidgets.QApplication(sys.argv)
#    QtWidgets.QMessageBox.critical(None, "OpenGL hellogl",
#            "PyOpenGL must be installed to run this example.")
#    sys.exit(1)

import qacl_stl as qacl
import qcalibration
import qconsole
import qvideogl
import qpanel
import qarm
import qrelay
import qnav

class Kameleon_GUI(QtWidgets.QMainWindow):
    front_mode = 1
    back_mode = 1
    angle = 0
    back_angle = 0
    front_angle = 0
    power = 0
    power_mode = 1

    servo_x = 90
    servo_y = 90

    def keyPressEvent(self, event):
        text = event.text()
        
        if text == 'w':
            #up
            self.servo_y+=5
        elif text == 's':
            #down
            self.servo_y-=5
        elif text == 'a':
            #left
            self.servo_x-=5
        elif text == 'd':
            #right
            self.servo_x+=5

        if self.servo_y < 0:
            self.servo_y = 0
        
        if self.servo_y > 180:
            self.servo_y = 180

        if self.servo_x < 0:
            self.servo_x = 0
        
        if self.servo_x > 180:
            self.servo_x = 180

        self.ui.servoX.setText(str(self.servo_x-90))
        self.ui.servoY.setText(str(self.servo_y-90))
        #self.qradio.put(0x02, 0x0d, [self.servo_x, self.servo_y])
        self.qradio.setval.emit(2,13, [self.servo_x, self.servo_y])


    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)

        

        self.ui = Ui_Form()
        self.ui.setupUi(self)

        

        self.qjoystick = QJoystick()
        self.qradio = QRadio()
       

        self.ui.frontNormal.clicked.connect(self.frontNormal)
        self.ui.frontLocked.clicked.connect(self.frontLocked)
        self.ui.frontReverse.clicked.connect(self.frontReverse)

        self.ui.backNormal.clicked.connect(self.backNormal)
        self.ui.backLocked.clicked.connect(self.backLocked)
        self.ui.backReverse.clicked.connect(self.backReverse)

        self.ui.drivingNormal.clicked.connect(self.drivingNormal)
        self.ui.drivingReverse.clicked.connect(self.drivingReverse)

        self.ui.navButton.clicked.connect(self.navOpen)

        self.__init__driving()
        self.__init__arm()

        #self.connect_signals_slots()
        self.qjoystick.axis_update.connect(self.axis_update)
        self.qjoystick.button_down.connect(self.button_down)
        
        

        self.qjoystick.start()
        self.qradio.start()

        #self.cam1_init()
        #self.cam2_init()

        self.acl_init()

        self.cam1GL = qvideogl.Ui_CameraCanvas(config.stream['cam1']['ip'], config.stream['cam1']['port'], config.stream['cam1']['resolution'])
        #self.cam1GL.resizeGL(config.stream['cam1']['resolution'][0], config.stream['cam1']['resolution'][1])
        self.cam1GL.setParent(self.ui.cam1GL)

        self.cam2GL = qvideogl.Ui_CameraCanvas(config.stream['cam2']['ip'], config.stream['cam2']['port'], config.stream['cam2']['resolution'])
        #self.cam2GL.resizeGL(config.stream['cam2']['resolution'][0], config.stream['cam2']['resolution'][1])
        self.cam2GL.setParent(self.ui.cam2GL)

        self.ui.calibrationOpen.clicked.connect(self.calibrationOpen)
        self.ui.consoleOpen.clicked.connect(self.consoleOpen)

        self.qpanel = qpanel.QPanel(config.panel['port'], config.panel['speed'], config.panel['calibration'])
        self.qpanel.start()

        self.qarm = qarm.Qarm(self.qpanel, self.qradio, self.ui)
        self.qarm.start()


        self.qrelay = qrelay.Qrelay(self.qradio, self.ui)

        self.ui.boxClose.clicked.connect(self.boxClose)
        self.ui.boxOpen.clicked.connect(self.boxOpen)

    #packet = self.id + b'\x00' + b'\x03' + (index+1).to_bytes(length=1, byteorder='big')+b'\x00'+b'\x00'
    #packet = self.id + b'\x00' + b'\x03' + (index+1).to_bytes(length=1, byteorder='big')+dir.to_bytes(length=1, byteorder='big')+pwm.to_bytes(length=1, byteorder='big')
    def boxClose(self):
        self.qradio.put_confrim(0x02, 0x03, [9, 1, 64])
        pass

    def boxOpen(self):
        self.qradio.put_confrim(0x02, 0x03, [9, 0, 64])
        pass

    def calibrationOpen(self):
        self.calibration = qcalibration.Calibration_GUI(self.qradio)
        self.calibration.show()
        pass

    def navOpen(self):
        self.nav = qnav.Nav_GUI(self.qradio, self)
        self.nav.show()
        #self.qradio.inputs10cmd2.connect(self.nav.update)

    def consoleOpen(self):
        self.console = qconsole.Console_GUI(self.qradio)
        self.console.show()
        pass

    def acl_init(self):
        file = open("acl.stl", "r") 
        data = file.read()
        self.acl = qacl.ALCWidget(xslider=self.ui.aclXslider, yslider=self.ui.aclYslider, xtext=self.ui.aclX, ytext=self.ui.aclY, ztext=self.ui.aclZ)
        #self.acl.resizeGL(380, 380)
        self.acl.setParent(self.ui.aclView)
        #print(self.qjoystick.axis_update)
        #print(self.qradio.inputs['acl'])
        #self.qradio.inputs['acl']
        self.qradio.inputs10cmd1.connect(self.acl.setRotation)
        #self.ui.aclView.addChildWidget(self.acl)


    def cam1_init(self):
        self.qvideo1Scene = QtWidgets.QGraphicsScene(self)    
        self.ui.cam1View.setScene(self.qvideo1Scene)
        self.qvideo1Frame = QVideoFrame()
        self.qvideo1Scene.addItem(self.qvideo1Frame) 
        self.qvideo1 = QVideo(config.stream['cam1']['ip'], config.stream['cam1']['port'], config.stream['cam1']['resolution'])
        self.qvideo1.image_update.connect(self.qvideo1Frame.setimage)
        self.qvideo1.start()

    def cam2_init(self):
        '''self.qvideo2Scene = QtWidgets.QGraphicsScene(self)    
        self.qvideo2Pixmap = QtGui.QPixmap("./images/power.png")
        self.qvideo2Image = QtWidgets.QGraphicsPixmapItem(self.qvideo2Pixmap)
        self.qvideo2Scene.addItem(self.qvideo2Image) 
        self.ui.cam2View.setScene(self.qvideo2Scene)
        
        self.qvideo2 = QVideo(config.stream['cam2']['ip'], config.stream['cam2']['port'])
        self.qvideo2.image_update.connect(self.cam2_update)
        self.qvideo2.start()'''
        self.qvideo2Scene = QtWidgets.QGraphicsScene(self)    
        self.ui.cam2View.setScene(self.qvideo2Scene)
        self.qvideo2Frame = QVideoFrame()
        self.qvideo2Scene.addItem(self.qvideo2Frame) 
        self.qvideo2 = QVideo(config.stream['cam2']['ip'], config.stream['cam2']['port'], config.stream['cam1']['resolution'])
        self.qvideo2.image_update.connect(self.qvideo2Frame.setimage)
        self.qvideo2.start()
        pass

    

    def cam2_update(self, image):
            #print(image)
        #print('cam1_update')
        image = image.scaled(640, 480)
        image = QtGui.QPixmap.fromImage(image)
        self.qvideo2Image.setPixmap(image)
        #self.qvideo2Pixmap.fromImage(image)
        #self.qvideo1Image.
        pass

    def frontNormal(self, event):
        self.front_mode = 1
        self.angles_update()
        self.wheel1.setPixmap(QtGui.QPixmap("./images/wheelnormal.png"))
        self.wheel2.setPixmap(QtGui.QPixmap("./images/wheelnormal.png"))

    def frontLocked(self, event):
        self.front_mode = 0
        self.angles_update()
        self.wheel1.setPixmap(QtGui.QPixmap("./images/wheellocked.png"))
        self.wheel2.setPixmap(QtGui.QPixmap("./images/wheellocked.png"))
    
    def frontReverse(self, event):
        self.front_mode = -1
        self.angles_update()
        self.wheel1.setPixmap(QtGui.QPixmap("./images/wheelreverse.png"))
        self.wheel2.setPixmap(QtGui.QPixmap("./images/wheelreverse.png"))

    def backNormal(self, event):
        self.back_mode = 1
        self.angles_update()
        self.wheel3.setPixmap(QtGui.QPixmap("./images/wheelnormal.png"))
        self.wheel4.setPixmap(QtGui.QPixmap("./images/wheelnormal.png"))

    def backLocked(self, event):
        self.back_mode = 0
        self.angles_update()
        self.wheel3.setPixmap(QtGui.QPixmap("./images/wheellocked.png"))
        self.wheel4.setPixmap(QtGui.QPixmap("./images/wheellocked.png"))
    
    def backReverse(self, event):
        self.back_mode = -1
        self.angles_update()
        self.wheel3.setPixmap(QtGui.QPixmap("./images/wheelreverse.png"))
        self.wheel4.setPixmap(QtGui.QPixmap("./images/wheelreverse.png"))

    def drivingNormal(self, event):
        self.power_mode = 1
        self.power_update()

    def drivingReverse(self, event):
        self.power_mode = -1
        self.power_update()

    def __init__driving(self):
        self.drivingScene = QtWidgets.QGraphicsScene(self)    

        self.power_image1 = QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("./images/power.png"))
        self.power_image1.setX(2)
        #self.power_image.
        self.power_image1.setOffset(0, 343)
        self.power_image1.setTransformOriginPoint(QtCore.QPointF(40/2 , 333/2))

        self.power_image2 = QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("./images/power.png"))
        self.power_image2.setX(153)
        #self.power_image.
        self.power_image2.setOffset(0, 343)
        self.power_image2.setTransformOriginPoint(QtCore.QPointF(40/2 , 333/2))
        #self.power_image.setboundingRect(QtCore.QRectF(QtCore.QPointF(100.0, 200.1), QtCore.QSizeF(11.2, 16.3)))
        self.drivingScene.addItem(self.power_image1)
        self.drivingScene.addItem(self.power_image2)

        self.drivingScene.addItem(QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("./images/driving.png"))) 
        
        self.wheel1=QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("./images/wheelnormal.png"))
        self.wheel1.setX(5)
        self.wheel1.setY(6)
        self.wheel1.setTransformOriginPoint(QtCore.QPointF(33/2 , 63/2))

        self.wheel2=QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("./images/wheelnormal.png"))
        self.wheel2.setX(156)
        self.wheel2.setY(6)
        self.wheel2.setTransformOriginPoint(QtCore.QPointF(33/2 , 63/2))

        self.wheel3=QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("./images/wheelnormal.png"))
        self.wheel3.setX(5)
        self.wheel3.setY(266)
        self.wheel3.setTransformOriginPoint(QtCore.QPointF(33/2 , 63/2))

        self.wheel4=QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("./images/wheelnormal.png"))
        self.wheel4.setX(156)
        self.wheel4.setY(266)
        self.wheel4.setTransformOriginPoint(QtCore.QPointF(33/2 , 63/2))

        self.drivingScene.addItem(self.wheel1) 
        self.drivingScene.addItem(self.wheel2) 
        self.drivingScene.addItem(self.wheel3) 
        self.drivingScene.addItem(self.wheel4) 
        self.drivingScene.setSceneRect(QtCore.QRectF(QtCore.QPointF(95.0, 160), QtCore.QSizeF(11.2, 16.3)))
        self.ui.drivingView.setScene(self.drivingScene)
        
        #self.ui.drivingView.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff))
        #self.ui.drivingView.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        #self.ui.drivingView.wheelEvent = lambda : None

    def __init__arm(self):
        xoffset = 220-50

        self.armScene = QtWidgets.QGraphicsScene(self)    
        self.arm_image_base = QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("./images/armbase.png"))
        self.arm_image_base.setTransform(QtGui.QTransform.fromScale(0.4, 0.4), True)
        self.arm_image_base.setOffset(-100+xoffset, 220)

        self.arm_image_1 = QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("./images/arm1.png"))
        self.arm_image_1.setTransform(QtGui.QTransform.fromScale(0.4, 0.4), True)
        self.arm_image_1.setX(-70+xoffset*0.4)
        self.arm_image_1.setY(33)
        self.arm_image_1.setTransformOriginPoint(QtCore.QPointF(900, 166))


        self.arm_image_2 = QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("./images/arm2.png"))
        self.arm_image_2.setTransform(QtGui.QTransform.fromScale(0.4, 0.4), True)
        self.arm_image_2.setX(-375+xoffset*0.4)
        self.arm_image_2.setY(46)
        self.arm_image_2.setTransformOriginPoint(QtCore.QPointF(848, 132))


        self.arm_image_3 = QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("./images/arm3.png"))
        self.arm_image_3.setTransform(QtGui.QTransform.fromScale(0.4, 0.4), True)
        self.arm_image_3.setX(-455+xoffset*0.4)
        self.arm_image_3.setY(46)
        self.arm_image_3.setTransformOriginPoint(QtCore.QPointF(347, 132))


        self.armScene.addItem(self.arm_image_base) 
        self.armScene.addItem(self.arm_image_1) 
        self.armScene.addItem(self.arm_image_2)
        self.armScene.addItem(self.arm_image_3)  
        self.armScene.setSceneRect(QtCore.QRectF(QtCore.QPointF(0, 0), QtCore.QSizeF(11.2, 16.3)))
        self.ui.armView.setScene(self.armScene)

        self.armPoint = QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("./images/armpoint.png"))
        self.armPoint.setTransform(QtGui.QTransform.fromScale(0.4, 0.4), True)
        self.armPoint.setX(-70+xoffset*0.4)
        self.armPoint.setY(33)

        self.arm_kinematics(24.8, 85.7, -110.5)
        self.qradio.inputs2cmd0.connect(self.arm_update)


    def arm_update(self, packet):
        angle1 = int.from_bytes(packet[0:2], byteorder='big')
        angle2 = int.from_bytes(packet[2:4], byteorder='big')
        angle3 = int.from_bytes(packet[4:6], byteorder='big')
        angle4 = int.from_bytes(packet[6:8], byteorder='big')

        angle1 = (angle1-4096)/22.7555555556
        angle2 = (angle2-4096)/22.7555555556
        angle3 = (angle3-4096)/22.7555555556
        angle4 = (angle4-4096)/22.7555555556

        self.ui.armS1.setText(str(round(angle1,1)))
        self.ui.armS2.setText(str(round(angle2,1)))
        self.ui.armS3.setText(str(round(angle3,1)))
        self.ui.armS4.setText(str(round(angle4,1)))

        self.arm_kinematics(angle2, angle3, angle4)
        pass        
        

    def arm_kinematics(self, angle1, angle2, angle3):
        
        
        angle1rad=math.radians(angle1)
        angle2rad=math.radians(angle2)
        angle3rad=math.radians(angle3)

        x3 = (config.arm['length1']*math.cos(angle1rad)) + (config.arm['length2']*math.cos(angle1rad + angle2rad)) + (config.arm['length3'] * math.cos(angle1rad + angle2rad + angle3rad))
        y3 = (config.arm['length1']*math.sin(angle1rad)) + (config.arm['length2']*math.sin(angle1rad + angle2rad)) + (config.arm['length3'] * math.sin(angle1rad + angle2rad + angle3rad))

        #821.0
        #angle1=angle1-155

        x1=config.arm['length1']*math.cos(math.radians(angle1))
        x1offset = -(x1-821)
        #0.0
        y1=config.arm['length1']*math.sin(math.radians(angle1))
        y1offset = -(y1-0.0)

        angle2=angle2+angle1

        x2=x1+config.arm['length2']*math.cos(math.radians(angle2))        
        x2offset = -(x2-1521)

        y2=y1+config.arm['length2']*math.sin(math.radians(angle2))
        y2offset = -(y2-0.0)


        '''
        x4 
        y4 = 
        '''

        
        

        self.ui.armX.setText(str(round(x3, 2)))
        self.ui.armY.setText(str(round(y3, 2)))
        #angle2=angle2-360*angle2//360

        angle3=angle2+angle3
        #print(angle3)

        self.arm_image_1.setRotation(angle1)

        

        self.arm_image_2.setOffset(x1offset, y1offset)
        self.arm_image_2.setTransformOriginPoint(QtCore.QPointF(848+x1offset, 132+y1offset))
        self.arm_image_2.setRotation(angle2)
        

        self.arm_image_3.setOffset(x2offset, y2offset)
        self.arm_image_3.setTransformOriginPoint(QtCore.QPointF(347+x2offset, 132+y2offset))
        self.arm_image_3.setRotation(angle3)
        
        self.armPoint.setOffset(x3, y3)

    def button_down(self, joy, button):
        print("button_down joy:{} button: {}".format(joy, button))
        if button == 0:
            self.reverseAll()
        elif button == 2:
            self.frontSwitch()
        elif button == 1:
            self.backSwitch()
        elif button == 4:
            self.powerSwitch()

    def frontSwitch(self):
        if self.front_mode == 1:
            self.front_mode = 0
            self.ui.frontLocked.click()
        elif self.front_mode == 0:
            self.front_mode = -1
            self.ui.frontReverse.click()
        elif self.front_mode == -1:
            self.front_mode = 1
            self.ui.frontNormal.click()

    def backSwitch(self):
        if self.back_mode == 1:
            self.back_mode = 0
            self.ui.backLocked.click()
        elif self.back_mode == 0:
            self.back_mode = -1
            self.ui.backReverse.click()
        elif self.back_mode == -1:
            self.back_mode = 1
            self.ui.backNormal.click()

    def powerSwitch(self):
        if self.power_mode == 1:
            self.power_mode = -1
            self.ui.drivingReverse.click()
        else:
            self.power_mode = 1
            self.ui.drivingNormal.click()

    def reverseAll(self):
        if self.front_mode == 1:
            self.front_mode = -1
            self.ui.frontReverse.click()
        elif self.front_mode == -1:
            self.front_mode = -1
            self.ui.frontNormal.click()

        if self.back_mode == 1:
            self.back_mode = -1
            self.ui.backReverse.click()
        elif self.back_mode == -1:
            self.back_mode = -1
            self.ui.backNormal.click()

        if self.power_mode == 1:
            self.power_mode = -1
            self.ui.drivingReverse.click()
        elif self.power_mode == -1:
            self.power_mode = -1
            self.ui.drivingNormal.click()
        pass


    def axis_update(self, joy, axe, value):
        #print("axis_update joy:{} axe: {} value: {}".format(joy, axe, value))
        if axe == 0:
            self.angle = config.driving['max_angle'] * value
            #if self.angle > config.driving['max_angle']:
            #    self.angle = config.driving['max_angle']
            #elif self.angle < config.driving['max_angle']*-1:
            #    self.angle = config.driving['max_angle']*-1
            
            #self.qradio.setval.emit('angle', self.angle)
            self.angles_update()
        elif axe == 1:
            #print(value)
            #value=value*0.75
            self.power = -value*self.power_mode      
            self.power_update()
    
    def power_update(self):
        if self.power == 0:
            self.power_image1.setOffset(0, 343)
            self.power_image2.setOffset(0, 343)
        elif self.power > 0:
            self.power_image1.setRotation(0)
            self.power_image1.setOffset(0, 343-343*self.power)

            self.power_image2.setRotation(0)
            self.power_image2.setOffset(0, 343-343*self.power)
        elif self.power < 0:
            self.power_image1.setRotation(180)
            self.power_image1.setOffset(0, 343+343*self.power)

            self.power_image2.setRotation(180)
            self.power_image2.setOffset(0, 343+343*self.power)
        
        self.driving_update()
        pass

    def angles_update(self):
        self.front_angle=self.angle*self.front_mode
        self.back_angle=self.angle*self.back_mode

        
        self.wheel1.setRotation(self.front_angle)
        self.wheel2.setRotation(self.front_angle)

        self.wheel3.setRotation(self.back_angle)
        self.wheel4.setRotation(self.back_angle)
        self.driving_update()

    def driving_update(self):
        front_angle=int(self.front_angle+config.driving['angle'])
        back_angle=int(self.back_angle+config.driving['angle'])
        power = int(self.power*255)
        if self.power > 0:
            direction = 0
        elif self.power < 0:
            direction = 1
            power*=-1
        else:
            direction = 3

        #print(front_angle)
        #print(back_angle)
        #print(power)

        if front_angle == 90 and back_angle == 90 and power == 0:
            self.qradio.setval.emit(1,0, [])
            self.qradio.setval.emit(1,1, [0])
        else:
            self.qradio.setval.emit(1,1, [])
            self.qradio.setval.emit(1,0, [front_angle, back_angle, power, direction])

        pass

if __name__ == "__main__":
    #QtGui.QPla
    app = QtWidgets.QApplication(sys.argv)
    myapp = Kameleon_GUI()
    myapp.show()
    sys.exit(app.exec_())
