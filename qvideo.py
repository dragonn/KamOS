import time
import cv2
from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import numpy as np
#sudo mjpg_streamer -i "input_uvc.so -d /dev/video0 -r 1280x720 -f 25"
class QVideoFrame(QtWidgets.QGraphicsItem):
    def __init__(self):
        super(QVideoFrame, self).__init__()

        #self.parent = parent
        #self.setParentItem(parent)
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateimage)
        self.timer.start(1000/15)
        self.img = QtGui.QImage(640, 480, QtGui.QImage.Format_RGB888)
        self.img.fill(QtGui.QColor(255, 255, 0))
        self.pixmap = QtGui.QPixmap(640, 480)
    def setimage(self, frame):
        #print(frame)
        #print(self.img.bits())
        #ptr=self.img.bits()
        #ptr.setsize(sys.getsizeof(frame))
        #byteArray = np.frombuffer(frame, np.uint8).reshape((1280*720)*3)
        #ptr.assign(byteArray)
        #print(self.img.loadFromData(frame, 'JPG'))
        #self.img.fromData(frame)
        #self.img.fill(QtGui.QColor(255, 0, 0))
        #self.img=QtGui.QImage(frame,1280,720,QtGui.QImage.Format_RGB888).rgbSwapped() 
        self.img=frame
        #self.pixmap.fromImage(self.img)
        #self.img.scaled(640, 480)
        #print("setimage")
        pass

    def updateimage(self):
        #print('QTimer')
        self.update()
        pass

    def sizeHint(self):
        return QtCore.QSize(640, 480)

    def minimumSizeHint(self):
        return  QtCore.QSize(50, 50)

    def boundingRect(self):
        return QtCore.QRectF(0, 0, 640, 480)

    def paint(self, painter, ignore, ignore2):
        painter.drawImage(0,0, self.img, 0, 0, 640, 480)
        #painter.drawPixmap(0, 0, self.pixmap)

class QVideo(QtCore.QThread):
    image_update = QtCore.pyqtSignal(QtGui.QImage)
    def __init__(self,address,port, resolution):
        QtCore.QThread.__init__(self)
        self.ip = address
        self.port = port
        self._exiting = False
        self.resolution = resolution

    def __del__(self):
        self._exiting = True
        self.wait()


    def open_video(self):
        connected = False
        while not connected:
            try:
                self.cap = cv2.VideoCapture("http://"+ str(self.ip) +":"+str(self.port)+"/?action=stream?dummy=param.mjpg")
                if self.cap.isOpened():
                    connected = True
                else:
                    time.sleep(0.2)
            except Exception:
                print("open_video error")
                time.sleep(0.2)
                pass

    def run(self):
        #cap = cv2.VideoCapture("http://"+ str(self.ip) +
        #    ":"+str(self.port)+"/?action=stream?dummy=param.mjpg")
        self.open_video()
        while not self._exiting:
            try:
                while self.cap.isOpened() and not self._exiting:
                    _,frame = self.cap.read()
                    # adjust width en height to the preferred values
                    image = QtGui.QImage(frame.tostring(),self.resolution[0],self.resolution[1],QtGui.QImage.Format_RGB888).rgbSwapped() 
                
                    #print(image)
                    #self.image_update.emit(image)
                    self.image_update.emit(image)
                    
            except Exception:
                #image = QtGui.QPixmap(1280, 720).toImage()
                #self.image_update.emit(b'')
                self.open_video()
        
