import time
import socket
import config
import threading
import queue
import crcmod.predefined
from cobs import cobs

from PyQt5 import QtCore
from threading import Lock
class QRadio(QtCore.QThread):
    setval = QtCore.pyqtSignal(int, int, list)
    setbytes = QtCore.pyqtSignal(int, int, bytes)
    
    queue = queue.Queue()
    confrim_count = 0
    confrim_queue = {}
    confrim_lock = Lock()
    buffer = b''
    #acl
    inputs2cmd0 = QtCore.pyqtSignal(bytes)
    inputs10cmd1 = QtCore.pyqtSignal(bytes)
    inputs10cmd2 = QtCore.pyqtSignal(bytes)
    inputs255cmd0 = QtCore.pyqtSignal(bytes)
    inputs254cmd1 = QtCore.pyqtSignal(bytes)
    
    always1cmd0 = [0x5a, 0x5a, 0x00, 0x00]
    always1cmd1 = []
    always2cmd0 = []
    always2cmd13 = []

    packetolds = {

    }
    def __init__(self):
        QtCore.QThread.__init__(self)
        self.crc16 = crcmod.predefined.mkCrcFun('modbus')
        self._exiting = False
        self.setval.connect(self.set)
        self.setbytes.connect(self.set_bytes)
        
        self.inputs254cmd1.connect(self.del_confrim)
    def set(self, id, cmd, value):
        setattr(self, 'always'+str(id)+'cmd'+str(cmd), value)
        #print("set id:{} cmd: {}".format(id, cmd))

    def set_bytes(self, id, cmd, value):
        setattr(self, 'always'+str(id)+'cmd'+str(cmd), value)
        #print("set id:{} cmd: {}".format(id, cmd))

    def put(self, id, cmd, values):
        self.queue.put_nowait({
            'id': id,
            'cmd': cmd,
            'values': values,
        })

    def put_confrim(self, id, cmd, values, retries=40):
        self.confrim_lock.acquire()
        self.confrim_count+=1
        self.confrim_queue[self.confrim_count]={
            'id': id,
            'cmd': cmd,
            'values': values,
            'retries': retries,
        }

        if self.confrim_count == 255:
            self.confrim_count=0
        
        self.confrim_lock.release()

    def del_confrim(self, bytes):
        self.confrim_lock.acquire()
        try:
            key = bytes[0]
            del self.confrim_queue[key]
        except Exception:
            pass
        finally:
            self.confrim_lock.release()
    def __del__(self):
        '''
        We are leaving now ...
        '''
        self._exiting = True
        self.wait()

    def run(self):
        self.open()
        read_thread = threading.Thread(target=self._read, args=())
        read_thread.start()
        while not self._exiting:
            try:
                #self.buffer+=self.read()
                #index=self.buffer.find(b'\x00')
                #print(index)
                #print(self.buffer)
                #print(index)
                #if index != -1:
                #    self.decode(self.buffer[:index])
                #    self.buffer=self.buffer[index+1:]
                    #print('clean')
                
                if self.always1cmd0 != []:
                    self.write(self.encode(1, 0, self.always1cmd0))
                    time.sleep(0.02)    

                if self.always1cmd1 != []:
                    self.write(self.encode(1, 1, self.always1cmd1))
                    time.sleep(0.02)    

                if self.always2cmd0 != []:
                    self.write(self.encode(2, 0, self.always2cmd0, bytes_enabled=True))
                

                if self.always2cmd13 != []:
                    self.write(self.encode(2, 13, self.always2cmd13))

                if not self.queue.empty():
                    packet = self.queue.get()
                    self.write(self.encode(packet['id'], packet['cmd'], packet['values'],  oldcount=20))

                if self.confrim_queue != {}:
                    self.confrim_lock.acquire()
                    for key in list(self.confrim_queue.keys()):
                        packet = self.confrim_queue[key]
                        #print(packet)
                        if packet['retries'] > 0:
                            self.write(self.encode(0xFE, 0, [key, packet['retries'], packet['id'], 0x00, packet['cmd']]+packet['values']))
                            packet['retries']-=1
                        else:
                            print("packet send timeout, dropping")
                            del self.confrim_queue[key]
                    self.confrim_lock.release()

                time.sleep(0.02)     
            except IOError as e:   
                print(e)
                self.open()
            except Exception as e:
                print(e)
            
    def _read(self):
        while not self._exiting:
            self.buffer+=self.read()
            index=self.buffer.find(b'\x00')
            if index != -1:
                self.decode(self.buffer[:index])
                self.buffer=self.buffer[index+1:]
            else:
                time.sleep(0.005)

    def encode(self, id, cmd, values, bytes_enabled=False, oldcount = 5):
        if not id in self.packetolds:
            self.packetolds[id]={}

        if not cmd in self.packetolds[id]:
            self.packetolds[id][cmd]={
                'old_count': 0,
                'values': values
            }

        if self.packetolds[id][cmd]['values'] != values or self.packetolds[id][cmd]['old_count'] > oldcount:
            
            
            message=id.to_bytes(1, byteorder='big')+0x00.to_bytes(1, byteorder='big')+cmd.to_bytes(1, byteorder='big')
            if bytes_enabled:
                message+=values
            else:
                message+=bytes(values)
            
            message+=self.crc16(message).to_bytes(2, byteorder='big')

            self.packetolds[id][cmd]['values']=values
            self.packetolds[id][cmd]['old_count']=0
            return cobs.encode(message)+b'\x00'
        else:
            self.packetolds[id][cmd]['old_count']+=1
            return b''

    def decode(self, message):
        try:
            message = cobs.decode(message)
            #print(''.join('{:02x} '.format(x) for x in message))
            id = message[0]
            cmd = message[1]
            crc = message[-4:-2]
            rssi = message[-2:]
            #print('WTF')
            #print(message[:-2])
            #crccalc=self.crc16(message[:-2]).to_bytes(2, byteorder='big')
            #print(''.join('{:02x} '.format(x) for x in crccalc))
            if crc == self.crc16(message[:-4]).to_bytes(2, byteorder='big'):
                getattr(self, 'inputs'+str(id)+'cmd'+str(cmd)).emit(message[2:-2])
            else:
                print(message)
                print("CRC decode error")
        except Exception as e:
            print(e)
