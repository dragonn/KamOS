import time
import serial
import config
import cobs

from PyQt5 import QtCore

import qradio_base

class QRadio(qradio_base.QRadio):
    def __init__(self):
        qradio_base.QRadio.__init__(self)

    def open(self):
        connected = False
        while not connected:
            try:
                self.serial = serial.Serial(config.radio['portserial'], config.radio['speed'])
                connected = True
            except Exception as e:
                print(e)
                print(config.radio['portserial'])
                print("open_serial error")
                time.sleep(1)

    def read(self):
        inWaiting = self.serial.inWaiting()
        if inWaiting > 0:
            return self.serial.read(inWaiting)
        else:
            return b''    

    def write(self, message):
        self.serial.write(message)