import sys
import math

from PyQt5.QtCore import pyqtSignal, QPoint, QSize, Qt
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import (QApplication, QHBoxLayout, QOpenGLWidget, QSlider,
                             QWidget)

import OpenGL.GL as gl
import time
import math

from array import array
class STL():
    def __init__(self, fname):
        self.max = -math.inf
        self.min = math.inf
        self.facets=[]
        with open(fname) as f: 
            lines = f.readlines()
            del lines[0]
            i=0
            while i < len(lines):
                line = lines[i]
                if line.startswith('  facet'):
                    facet = {
                        'normal': {

                        }
                    }
                    normal = line.lstrip(' ').split(' ')
                    facet['normal']['x']=float(normal[2])
                    facet['normal']['y']=float(normal[3])
                    facet['normal']['z']=float(normal[4])
                    #print(normal)
                    vertex1=lines[i+2].lstrip(' ').split(' ')
                    vertex2=lines[i+3].lstrip(' ').split(' ')
                    vertex3=lines[i+4].lstrip(' ').split(' ')
                    #print(vertex1)
                    #print(vertex2)
                    #print(vertex3)
                    facet[1]={
                        'x': float(vertex1[1]),
                        'y': float(vertex1[2]),
                        'z': float(vertex1[3]),
                    }
                    facet[2]={
                        'x': float(vertex2[1]),
                        'y': float(vertex2[2]),
                        'z': float(vertex2[3]),
                    }
                    facet[3]={
                        'x': float(vertex3[1]),
                        'y': float(vertex3[2]),
                        'z': float(vertex3[3]),
                    }
                    for f in range(1, 3):
                        if facet[f]['x'] > self.max:
                            self.max=facet[f]['x']
                        if facet[f]['y'] > self.max:
                            self.max=facet[f]['y']
                        if facet[f]['z'] > self.max:
                            self.max=facet[f]['z']

                        if facet[f]['x'] < self.min:
                            self.min=facet[f]['x']
                        if facet[f]['y'] < self.min:
                            self.min=facet[f]['y']
                        if facet[f]['z'] < self.min:
                            self.min=facet[f]['z']

                    self.facets.append(facet)
                    #facet
                    i+=5
                else:
                    i+=1
        
    def normalize(self):
        #print(len(self.facets))
        for facet in self.facets:
            for f in range(1, 4):
                facet[f]['x']=(facet[f]['x']-self.min)/(self.max-self.min)*(1+1)-1
                facet[f]['y']=(facet[f]['y']-self.min)/(self.max-self.min)*(1+1)-1
                facet[f]['z']=(facet[f]['z']-self.min)/(self.max-self.min)*(1+1)-1

        
        pass
            
        

class ALCWidget(QOpenGLWidget):
    xRotationChanged = pyqtSignal(int)
    yRotationChanged = pyqtSignal(int)
    zRotationChanged = pyqtSignal(int)

    def __init__(self, parent=None, xslider=None, yslider=None, xtext=None, ytext=None, ztext=None):
        super(ALCWidget, self).__init__(parent)

        self.object = 0
        self.xRot = 0
        self.yRot = 0
        self.zRot = 0

        self.lastPos = QPoint()

        self.trolltechGreen = QColor.fromCmykF(0.40, 0.0, 1.0, 0.0)
        self.trolltechPurple = QColor.fromCmykF(0.39, 0.39, 0.0, 0.0)

        self.xslider = xslider
        self.yslider = yslider

        self.xtext = xtext
        self.ytext = ytext
        self.ztext = ztext

    def getOpenglInfo(self):
        info = """
            Vendor: {0}
            Renderer: {1}
            OpenGL Version: {2}
            Shader Version: {3}
        """.format(
            gl.glGetString(gl.GL_VENDOR),
            gl.glGetString(gl.GL_RENDERER),
            gl.glGetString(gl.GL_VERSION),
            gl.glGetString(gl.GL_SHADING_LANGUAGE_VERSION)
        )

        return info

    def minimumSizeHint(self):
        return QSize(50, 50)

    def sizeHint(self):
        return QSize(380, 380)

    def setRotation(self, angles):
        #print(angles)
        x=int.from_bytes(angles[0:2], byteorder='big', signed=True)
        #print(angles[0:1])
        y=int.from_bytes(angles[2:4], byteorder='big', signed=True)
        z=int.from_bytes(angles[4:6], byteorder='big', signed=True)
        #print('x:{} y:{} z:{}'.format(x, y, z))
        self.setXRotation(y)
        self.setYRotation(x)
        self.setZRotation(z)

        self.xslider.setValue(x)
        self.yslider.setValue(y)

        self.xtext.setText(str(x))
        self.ytext.setText(str(y))
        self.ztext.setText(str(z))

        self.update()

    def setXRotation(self, angle):
        #print('x: {}'.format(angle))
        #angle = self.normalizeAngle(angle)
        if angle != self.xRot:
            self.xRot = angle
            self.xRotationChanged.emit(angle)
            #self.update()

    def setYRotation(self, angle):
        #print('y: {}'.format(angle))
        #angle = self.normalizeAngle(angle)
        angle+=90
        if angle != self.yRot:
            self.yRot = angle
            self.yRotationChanged.emit(angle)
            #self.update()

    def setZRotation(self, angle):
        #print('z: {}'.format(angle))
        #angle = self.normalizeAngle(angle)
        if angle != self.zRot:
            self.zRot = angle
            self.zRotationChanged.emit(angle)
            #self.update()

    def initializeGL(self):
        print(self.getOpenglInfo())
        #self.stl=STL('cube.stl')
        self.stl=STL('acl.stl')
        self.stl.normalize()
        
        self.setClearColor(self.trolltechPurple.darker())
        self.object = self.makeObject()
        gl.glFrontFace(gl.GL_CW)
        gl.glShadeModel(gl.GL_SMOOTH)
        gl.glEnable(gl.GL_DEPTH_TEST)
        gl.glEnable(gl.GL_CULL_FACE)
        gl.glEnable(gl.GL_LIGHTING)
        
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_AMBIENT, [0.0, 0.0, 0.0, 1.0])
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_DIFFUSE, [0.7, 0.7, 0.7, 1.0])
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_SPECULAR, [0.7, 0.7, 0.7, 1.0])
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_POSITION, [1.0,100.0,1.0,0.0])
        gl.glEnable(gl.GL_LIGHT0)

        gl.glLightModelfv(gl.GL_LIGHT_MODEL_AMBIENT, [0.3, 0.3, 0.3, 1.0])
        gl.glLightModeli(gl.GL_LIGHT_MODEL_LOCAL_VIEWER, gl.GL_TRUE)
        gl.glEnable(gl.GL_CULL_FACE)
        gl.glCullFace(gl.GL_BACK)

        

        '''
        glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmb)
        glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDifAndSpec)
        glLightfv(GL_LIGHT0, GL_SPECULAR, lightDifAndSpec)
        glEnable(GL_LIGHT0)
        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globAmb)
        glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE)

        glEnable(GL_CULL_FACE)
        glCullFace(GL_BACK)
        '''
        #self.resizeGL(self.stl.max, self.stl.max)
        #gl.glScalef(0.5,0.5,0.5);
        #vert=[0.0,0.0,0.0,
        #    1.0,0.0,0.0,
        #    1.0,1.0,0.0,
        #    0.0,1.0,0.0]

        #ar=array("f",vert)
        #gl.glBufferData(gl.GL_ARRAY_BUFFER, ar.tostring(), gl.GL_STATIC_DRAW)
   
        self.lastTime = time.time()
        self.frames=0
    def paintGL(self):
        
        self.currentTime = time.time()
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        gl.glLoadIdentity()
        gl.glTranslated(0.0, 0.0, -10.0)
        gl.glRotated(self.xRot, 1.0, 0.0, 0.0)
        gl.glRotated(self.yRot, 0.0, 1.0, 0.0)
        gl.glRotated(self.zRot, 0.0, 0.0, 1.0)
        
        gl.glCallList(self.object)
        #gl.glMatrixMode (gl.GL_PROJECTION)
        self.frames+=1
        if self.currentTime - self.lastTime >= 1.0:
            #print('{} ms/frames'.format(1000/self.frames))
            self.frames=0
            self.lastTime=self.currentTime
        

    def resizeGL(self, width, height):
        side = min(width, height)
        if side < 0:
            return

        gl.glViewport((width - side) // 2, (height - side) // 2, side,
                           side)

        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        gl.glOrtho(-0.5, +0.5, +0.5, -0.5, 4.0, 15.0)
        gl.glMatrixMode(gl.GL_MODELVIEW)

    def mousePressEvent(self, event):
        self.lastPos = event.pos()

    def mouseMoveEvent(self, event):
        dx = event.x() - self.lastPos.x()
        dy = event.y() - self.lastPos.y()

        if event.buttons() & Qt.LeftButton:
            self.setXRotation(self.xRot + 8 * dy)
            self.setYRotation(self.yRot + 8 * dx)
        elif event.buttons() & Qt.RightButton:
            self.setXRotation(self.xRot + 8 * dy)
            self.setZRotation(self.zRot + 8 * dx)

        self.lastPos = event.pos()

    def makeObject(self):
        genList = gl.glGenLists(1)
        gl.glNewList(genList, gl.GL_COMPILE)

        gl.glBegin(gl.GL_TRIANGLES)
        #gl.glBegin(gl.GL_LINE_LOOP)
        #gl.glMatrixMode(gl.GL_MODELVIEW)
        #gl.glLoadIdentity()
        '''x1 = +0.06
        y1 = -0.14
        x2 = +0.14
        y2 = -0.06
        x3 = +0.08
        y3 = +0.00
        x4 = +0.30
        y4 = +0.22

        self.quad(x1, y1, x2, y2, y2, x2, y1, x1)
        self.quad(x3, y3, x4, y4, y4, x4, y3, x3)

        #self.extrude(x1, y1, x2, y2)
        self.extrude(x2, y2, y2, x2)
        self.extrude(y2, x2, y1, x1)
        self.extrude(y1, x1, x1, y1)
        self.extrude(x3, y3, x4, y4)
        self.extrude(x4, y4, y4, x4)
        self.extrude(y4, x4, y3, x3)

        #gl.glBegin(gl.GL_LINES);'''
        #print(len(self.stl.facets))
        #print(self.stl.facets)
        for facet in self.stl.facets:
            gl.glNormal3f(facet['normal']['x'], facet['normal']['y'], facet['normal']['z'])
            for f in range(1, 4):
                #print(f)
                vertex=facet[f]
                gl.glVertex3d(vertex['x']*0.3, vertex['y']*0.3, vertex['z']*0.3)
        

        '''NumSectors = 200

        for i in range(NumSectors):
            angle1 = (i * 2 * math.pi) / NumSectors
            x5 = 0.30 * math.sin(angle1)
            y5 = 0.30 * math.cos(angle1)
            x6 = 0.20 * math.sin(angle1)
            y6 = 0.20 * math.cos(angle1)

            angle2 = ((i + 1) * 2 * math.pi) / NumSectors
            x7 = 0.20 * math.sin(angle2)
            y7 = 0.20 * math.cos(angle2)
            x8 = 0.30 * math.sin(angle2)
            y8 = 0.30 * math.cos(angle2)

            self.quad(x5, y5, x6, y6, x7, y7, x8, y8)

            self.extrude(x6, y6, x7, y7)
            self.extrude(x8, y8, x5, y5)
        '''
        gl.glEnd()
        gl.glEndList()

        return genList

    def quad(self, x1, y1, x2, y2, x3, y3, x4, y4):
        self.setColor(self.trolltechGreen)

        gl.glVertex3d(x1, y1, -0.05)
        gl.glVertex3d(x2, y2, -0.05)
        gl.glVertex3d(x3, y3, -0.05)
        gl.glVertex3d(x4, y4, -0.05)

        gl.glVertex3d(x4, y4, +0.05)
        gl.glVertex3d(x3, y3, +0.05)
        gl.glVertex3d(x2, y2, +0.05)
        gl.glVertex3d(x1, y1, +0.05)

    def extrude(self, x1, y1, x2, y2):
        self.setColor(self.trolltechGreen.darker(250 + int(100 * x1)))

        gl.glVertex3d(x1, y1, +0.05)
        gl.glVertex3d(x2, y2, +0.05)
        gl.glVertex3d(x2, y2, -0.05)
        gl.glVertex3d(x1, y1, -0.05)

    def normalizeAngle(self, angle):
        while angle < 0:
            angle += 360 * 16
        while angle > 360 * 16:
            angle -= 360 * 16
        return angle

    def setClearColor(self, c):
        gl.glClearColor(c.redF(), c.greenF(), c.blueF(), c.alphaF())

    def setColor(self, c):
        gl.glColor4f(c.redF(), c.greenF(), c.blueF(), c.alphaF())
