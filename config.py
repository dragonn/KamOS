driving = {
    'angle': 90,
    'max_angle': 30,
}

radio = {
    'driver': 'serial',
    
    'portserial': '/dev/ttyUSB0',
    'speed': 115200,
    
    'ip': '127.0.0.1',
    'port': 3307
}


#mjpg_streamer -i "/usr/lib/mjpg-streamer/input_uvc.so -d /dev/video0 -r 640x480 -f 24" -o "/usr/lib/mjpg-streamer/output_http.so -w ./www"
#ffmpeg -f v4l2 -i /dev/video0 -preset ultrafast -vcodec libx264 -tune zerolatency -b 900k -f h264 udp://127.0.0.1:5000
stream = {
    'cam1': {
        'ip': '192.168.0.100',
        'port': 8080,
        'resolution': [640, 480]
    },
    'cam2': {
        'ip': '192.168.0.100',
        'port': 8081,
        'resolution': [640, 480]
    }
}


panel = {
    'port': '/dev/ttyACM0',
    'speed': 115200,
    'calibration': {
        'x0': {
            'half': 127, 
            'dead': 7
        },
        'y0': {
            'half': 127,
            'dead': 7
        },

        'x1': {
            'half': 127,
            'dead': 7
        },
        'y1': {
            'half': 127,
            'dead': 7
        },

        'x2': {
            'half': 127,
            'dead': 7
        },
        'y2': {
            'half': 127,
            'dead': 7
        },

        'x3': {
            'half': 127,
            'dead': 7
        },
        'y3': {
            'half': 127,
            'dead': 7
        },
    }
}

arm = {
    'length1': 821,
    'length2': 700,
    'length3': 210,
}
