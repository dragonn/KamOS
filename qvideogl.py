from PyQt5.QtWidgets import QOpenGLWidget
import cv2
from OpenGL.GL import *
from OpenGL.GLU import *
import numpy as np
from PyQt5 import QtCore, QtGui, QtCore

import config
import numpy as np
import time


#def silent_error_handler(status, func_name, err_msg, file_name, line):
#    pass
#cv2.redirectError(silent_error_handler)

class Video(QtCore.QThread):
    def __init__(self,captureurl):
        QtCore.QThread.__init__(self)

        self.captureurl = captureurl
        #print("framerate: {}".format(self.capture.get(cv2.CAP_PROP_XI_FRAMERATE)))
        #print("buffersize: {}".format(self.capture.get(cv2.CAP_PROP_BUFFERSIZE)))
        #self.capture.set(cv2.CAP_PROP_BUFFERSIZE, 1); 
        #print("buffersize: {}".format(self.capture.get(cv2.CAP_PROP_BUFFERSIZE)))
        #self.currentFrame=None
        #cv2.CAP_PROP_BUFFERSIZE        
        self.capture=None
    def open(self):
        del self.capture
        self.capture = cv2.VideoCapture(self.captureurl)
        while not self.capture.isOpened():
            self.capture = cv2.VideoCapture(self.captureurl)
            time.sleep(0.1)

    def getFrame(self):
        return self.currentFrame

    def run(self):
        self.open()
        #self.capture = cv2.VideoCapture('udp://127.0.0.1:5000',cv2.CAP_FFMPEG)
        #time.sleep(1)
        print("run caputer")
        while True:
            self.status,currentFrame=self.capture.read()
            if self.status == False:                
                self.open()
            else:
                #self.currentFrame=cv2.rotate(self.currentFrame, cv2.ROTATE_90_CLOCKWISE)
                M=cv2.getRotationMatrix2D((640/2, 480/2), 180, 1)
                
                currentFrame=cv2.warpAffine(currentFrame, M, (640, 480))
                self.currentFrame=cv2.flip(currentFrame, +1, currentFrame)
                pass


                

class Ui_CameraCanvas(QOpenGLWidget):
    def __init__(self, address, port, resolution):
        QOpenGLWidget.__init__(self)
        self.ip = address
        self.port = port
        self.resolution = resolution
        self.video = Video("http://"+ str(self.ip) +":"+str(self.port)+"/?action=stream?dummy=param.mjpg")
        #self.video = Video("udp://"+ str(self.ip) +":"+str(self.port)+'/')
        self.video.start()
        self._timer = QtCore.QTimer(self)
        self._timer.timeout.connect(self.play)
        self._timer.setInterval(1)
        self._timer.start()
        self.update()
        self.qframe = None
        
    def paintGL(self):
        #print("paint")
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glClearColor (0.0,0.0,0.0,1.0)
        #if (self.qframe != None):
        #self.qframe = self.qframe.scaled(self.size(), QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation);
        glDisable(GL_DEPTH_TEST)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluOrtho2D(0,self.resolution[0],self.resolution[1],0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        glEnable(GL_TEXTURE_2D)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        #self.qframe.
        #ptr = self.qframe.bits() 
        #QGLBuffer.read(0, ptr, ptr.getsize)
        #glOrtho(0, self.resolution[1], self.resolution[0], 0, 1, -1)
        glTexImage2D( GL_TEXTURE_2D, 0, 4, self.resolution[0], self.resolution[1], 0, GL_BGR, GL_UNSIGNED_BYTE, self.qframe)
        #glRotate(90, 0, 0, 1)
        glBegin(GL_QUADS)
        glTexCoord2f(0,0); glVertex2f(0,self.resolution[1])
        glTexCoord2f(0,1); glVertex2f(0,0)
        glTexCoord2f(1,1); glVertex2f(self.resolution[0],0)
        glTexCoord2f(1,0); glVertex2f(self.resolution[0],self.resolution[1])
        glEnd()
        glDisable(GL_TEXTURE_2D)
        #glFlush()
    def resizeGL(self, w, h):      
        glViewport(0, 0, w, h)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(40.0, 1.0, 1.0, 30.0)    
        glMatrixMode (GL_MODELVIEW)
    def initializeGL(self):
        # set viewing projection
        glClearColor(0.0, 0.0, 0.0, 1.0)
        glClearDepth(1.0)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(40.0, 1.0, 1.0, 30.0)

    def resetCamera(self):
        try: 
            self._timer.timeout.disconnect() 
        except:
            pass
        del self.video

        pass
    def play(self):
        #print("timer")
        try:
            #self.video.captureNextFrame()
            self.qframe = self.video.getFrame()
            self.update()
        except Exception as e:
            #print(e)
            #print("No frame")
            pass

    def minimumSizeHint(self):
        return QtCore.QSize(50, 50)

    def sizeHint(self):
        return QtCore.QSize(640, 480)