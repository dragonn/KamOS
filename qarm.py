import time
import math
from PyQt5 import QtCore
from enum import Enum

class Mode(Enum):
    PWM = 0
    PID = 1
    KIN = 2
    IDLE = 3

class Qarm(QtCore.QThread):
    drill_left = False
    mode = Mode.PWM

    modepwm = {
        'pwms' :[0, 0, 0, 0, 0, 0, 0, 0],
        'directions': [0, 0, 0, 0, 0, 0, 0, 0],
    }

    modepid = {
        'multi': 20,
        'pids': [0, 0, 0, 0],
        'pwms': [0, 0, 0, 0],
        'directions': [0, 0, 0, 0],
    }


    modekin = {
        'multi': 50,
        'kin': [0, 0],
        'pwms': [0, 0, 0, 0, 0],
        'directions': [0, 0, 0, 0, 0],
    }

    packet = []
    def __init__(self, qpanel, qradio, ui):
        QtCore.QThread.__init__(self)

        self.qpanel=qpanel
        self.qradio=qradio
        self._exiting = False

        self.qpanel.bin1.connect(self.drill_left_enable)
        self.qpanel.bin3.connect(self.modeSWITCH)
        self.ui = ui

        self.ui.armPWM.clicked.connect(self.modePWM)
        self.ui.armPID.clicked.connect(self.modePID)
        self.ui.armKIN.clicked.connect(self.modeKIN)

    def modePWM(self):
        self.mode = Mode.PWM

    def modePID(self):
        self.mode = Mode.PID

    def modeKIN(self):
        self.mode = Mode.KIN

    def modeSWITCH(self, value):
        if value == 1:
            #print('swith mode')
            #print(self.mode)
            #b'\x02\x00\x00'
            self.qradio.put_confrim(0x02, 0xFF, [])
            if self.mode == Mode.PWM:
                self.mode = Mode.PID
                self.ui.armPID.click()
                return
            elif self.mode == Mode.PID:
                self.mode = Mode.KIN
                self.ui.armKIN.click()
                return
            elif self.mode == Mode.KIN:
                self.mode = Mode.PWM
                self.ui.armPWM.click()
                return

    def pwm_set(self, id, value):
		#if id == 2:
		#	value=value/2
		
        if value >= 127:
            self.modepwm['pwms'][id]=min(((value-127)*2), 255)
            self.modepwm['directions'][id]=0
            
            #if id == 2:
            #    self.modepwm['pwms'][id]=self.modepwm['pwms'][id]/2
        else:
            self.modepwm['pwms'][id]=(127-value)*2
            self.modepwm['directions'][id]=1
            #if id == 2:
            #    self.modepwm['pwms'][id]=self.modepwm['pwms'][id]/2
        pass

    def pid_set(self, id, value):
        if id < 4:
            if value >= 127:
                self.modepid['pids'][id]=round((min(((value-127)*2), 255)/255)*self.modepid['multi'])
            else:
                self.modepid['pids'][id]=round(((((127-value)*2)/255)*self.modepid['multi'])*-1)
        else:
            if value >= 127:
                self.modepid['pwms'][id-4]=min(((value-127)*2), 255)
                self.modepid['directions'][id-4]=0
            else:
                self.modepid['pwms'][id-4]=(127-value)*2
                self.modepid['directions'][id-4]=1


    def kin_set(self, id, value):
        if id == 2:
            self.modekin['kin'][0]=round((min(((value-127)*2), 255)/255)*self.modekin['multi'])
        elif id == 1:
            self.modekin['kin'][1]=round((min(((value-127)*2), 255)/255)*self.modekin['multi'])
        elif id == 0:
            if value >= 127:
                self.modekin['pwms'][0]=min(((value-127)*2), 255)
                self.modekin['directions'][0]=0
            else:
                self.modekin['pwms'][0]=(127-value)*2
                self.modekin['directions'][0]=1
        else:
            if value >= 127:
                self.modekin['pwms'][0]=min(((value-127)*2), 255)
                self.modekin['directions'][0]=0
            else:
                self.modekin['pwms'][0]=(127-value)*2
                self.modekin['directions'][0]=1
            
    def run(self):
        while not self._exiting:
            #print(self.modepwm['pwms'])
            #print(self.mode)
            #print(self.modepwm['directions'])

            if self.mode == Mode.PWM:
                self.pwm_set(2, self.qpanel.x0)
                self.pwm_set(1, self.qpanel.y0)

                self.pwm_set(0, 255-self.qpanel.x1)
                self.pwm_set(7, self.qpanel.y1)

                self.pwm_set(6, 255-self.qpanel.x2)
                self.pwm_set(3, self.qpanel.y2)

                self.pwm_set(4, self.qpanel.x3)
                self.pwm_set(5, self.qpanel.y3)
            
                
                
                
                
                '''
                self.pwm_set(1, self.qpanel.x1)
                self.pwm_set(2, self.qpanel.y0)
                self.pwm_set(3, self.qpanel.x0)
                self.pwm_set(4, self.qpanel.y2)
                self.pwm_set(5, self.qpanel.y3)
                self.pwm_set(6, self.qpanel.x3)
                self.pwm_set(7, self.qpanel.x2)
                '''
                
                #print(self.modepwm['directions'])
                if sum(1 for i in self.modepwm['pwms'] if i == 0) != 8:
                    dir = int(''.join('1' if i else '0' for i in self.modepwm['directions']), 2)
                    #print(''.join('1' if i else '0' for i in self.modepwm['directions']))    
                    #print(dir)
                    self.packet = [Mode.PWM.value] + self.modepwm['pwms'] + [dir]
                    #print(self.packet)
                    self.qradio.setbytes.emit(2,0, bytes(self.packet))
                else:
                    self.packet = [0xFF]
                    self.qradio.setbytes.emit(2,0, bytes(self.packet))
                
            elif self.mode == Mode.PID:
                self.pid_set(2, self.qpanel.x0)
                self.pid_set(1, self.qpanel.y0)

                self.pid_set(0, 255-self.qpanel.x1)
                self.pid_set(7, self.qpanel.y1)

                self.pid_set(6, 255-self.qpanel.x2)
                self.pid_set(3, self.qpanel.y2)

                self.pid_set(4, self.qpanel.x3)
                self.pid_set(5, self.qpanel.y3)

                #print(self.modepid['pwms'])

                #if sum(1 for i in self.modepid['pwms'] if i == 0) != 4 or sum(1 for i in self.modepid['pids'] if i == 0) != 4:
                dir = int(''.join('1' if i else '0' for i in self.modepid['directions']), 2)
                pids=b''
                for i in self.modepid['pids']:
                    pids+=i.to_bytes(2, byteorder='big', signed=True) 
                
                self.packet = Mode.PID.value.to_bytes(1, byteorder='big') + pids + bytes(self.modepid['pwms']) + dir.to_bytes(1, byteorder='big')
                self.qradio.setbytes.emit(2,0, self.packet)
                #else:
                #    self.packet = [0xFF]
                #    self.qradio.setbytes.emit(2,0, bytes(self.packet))
            elif self.mode == Mode.KIN:
                self.kin_set(2, self.qpanel.x0)
                self.kin_set(1, self.qpanel.y0)

                self.kin_set(0, self.qpanel.x1)
                self.kin_set(7, self.qpanel.y1)

                self.kin_set(6, self.qpanel.x2)
                #self.kin_set(3, self.qpanel.y2)

                #self.kin_set(4, self.qpanel.x3)
                self.kin_set(5, self.qpanel.y3)

                #if sum(1 for i in self.modekin['pwms'] if i == 0) != 4 or sum(1 for i in self.modekin['kin'] if i == 0) != 4:
                dir = int(''.join('1' if i else '0' for i in self.modekin['directions']), 2)
                kin = self.modekin['kin'][0].to_bytes(2, byteorder='big', signed=True)+self.modekin['kin'][1].to_bytes(2, byteorder='big', signed=True)
                #kin = bytes([i.to_bytes(2, byteorder='big', signed=True) for i in self.modekin['kin']])
                self.packet = Mode.KIN.value.to_bytes(1, byteorder='big') + kin + bytes(self.modepid['pwms']) + dir.to_bytes(1, byteorder='big')
                self.qradio.setbytes.emit(2,0, self.packet)
                #else:
                #    self.packet = [0xFF]
                #    self.qradio.setbytes.emit(2,0, bytes(self.packet))
                pass
            elif self.mode == Mode.IDLE:
                pass
            time.sleep(0.05)

    def drill_left_enable(self, value):
        self.drill_left = value

