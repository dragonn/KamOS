# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_nav.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Nav(object):
    def setupUi(self, Nav):
        Nav.setObjectName("Nav")
        Nav.resize(1254, 672)
        self.navView = QtWidgets.QGraphicsView(Nav)
        self.navView.setGeometry(QtCore.QRect(0, 0, 1260, 673))
        self.navView.setObjectName("navView")

        self.retranslateUi(Nav)
        QtCore.QMetaObject.connectSlotsByName(Nav)

    def retranslateUi(self, Nav):
        _translate = QtCore.QCoreApplication.translate
        Nav.setWindowTitle(_translate("Nav", "Form"))

