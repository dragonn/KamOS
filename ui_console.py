# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_console.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Console(object):
    def setupUi(self, Console):
        Console.setObjectName("Console")
        Console.resize(400, 300)
        self.sendCommand = QtWidgets.QLineEdit(Console)
        self.sendCommand.setGeometry(QtCore.QRect(3, 1, 321, 30))
        self.sendCommand.setText("")
        self.sendCommand.setObjectName("sendCommand")
        self.sendButton = QtWidgets.QPushButton(Console)
        self.sendButton.setGeometry(QtCore.QRect(329, 2, 71, 30))
        self.sendButton.setObjectName("sendButton")
        self.listCommand = QtWidgets.QListWidget(Console)
        self.listCommand.setGeometry(QtCore.QRect(4, 34, 391, 261))
        self.listCommand.setObjectName("listCommand")

        self.retranslateUi(Console)
        QtCore.QMetaObject.connectSlotsByName(Console)

    def retranslateUi(self, Console):
        _translate = QtCore.QCoreApplication.translate
        Console.setWindowTitle(_translate("Console", "Form"))
        self.sendButton.setText(_translate("Console", "Wyślij"))

