import sys
import math

from PyQt5.QtCore import pyqtSignal, QPoint, QSize, Qt
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import (QApplication, QHBoxLayout, QOpenGLWidget, QSlider,
                             QWidget)

import OpenGL.GL as gl
import time
import math


class CircleEvaluator:
        # this coded stolen from OpenGC
    # http://opengc.sourceforge.net/
    # Copyright (c) 2001-2004 Damion Shelton
    
    def __init__(self):
        self.m_XOrigin = 0
        self.m_YOrigin = 0
        self.m_StartArcDegrees = 0
        self.m_EndArcDegrees = 0
        self.m_DegreesPerPoint = 10    
        self.m_Radius = 1
        
    def SetOrigin(self,x,y):
        self.m_XOrigin = x
        self.m_YOrigin = y
        
    def SetArcStartEnd(self,startArc, endArc):
        self.m_StartArcDegrees = startArc
        self.m_EndArcDegrees = endArc
    
    def SetDegreesPerPoint(self,degreesPerPoint):
        self.m_DegreesPerPoint = degreesPerPoint

    def SetRadius(self,radius):
        if (radius > 0):
            self.m_Radius = radius
        else:
            self.m_Radius = 1
    
    def Evaluate(self):
        #We parametrically evaluate the circle
        #x = math.sin(t)
        #y = math.cos(t)
        #t goes from 0 to 2math.pi
        #0 degrees = 0rad, 90 degrees = math.pi/2rad, etc.
    
        startRad = self.m_StartArcDegrees / 180.0 * math.pi
        endRad = self.m_EndArcDegrees / 180.0 * math.pi
        radPerPoint = self.m_DegreesPerPoint / 180.0 * math.pi
        
        if (startRad > endRad):
            endRad += 2*math.pi
            
        currentRad = startRad
        
        while(currentRad < endRad):
            x = self.m_Radius*math.sin(currentRad) + self.m_XOrigin
            y = self.m_Radius*math.cos(currentRad) + self.m_YOrigin
            gl.glVertex2d(x,y)
            currentRad += radPerPoint
        
        x = self.m_Radius*math.sin(endRad) + self.m_XOrigin
        y = self.m_Radius*math.cos(endRad) + self.m_YOrigin
        gl.glVertex2d(x,y)
        

class ALCWidget(QOpenGLWidget):
    xRotationChanged = pyqtSignal(int)
    yRotationChanged = pyqtSignal(int)
    zRotationChanged = pyqtSignal(int)
    Roll = 20
    Pitch = 0

    def __init__(self, parent=None, xslider=None, yslider=None):
        super(ALCWidget, self).__init__(parent)

        self.object = 0
        self.xRot = 0
        self.yRot = 0
        self.zRot = 0

        self.lastPos = QPoint()

        self.trolltechGreen = QColor.fromCmykF(0.40, 0.0, 1.0, 0.0)
        self.trolltechPurple = QColor.fromCmykF(0.39, 0.39, 0.0, 0.0)

        self.xslider = xslider
        self.yslider = yslider

    def getOpenglInfo(self):
        info = """
            Vendor: {0}
            Renderer: {1}
            OpenGL Version: {2}
            Shader Version: {3}
        """.format(
            gl.glGetString(gl.GL_VENDOR),
            gl.glGetString(gl.GL_RENDERER),
            gl.glGetString(gl.GL_VERSION),
            gl.glGetString(gl.GL_SHADING_LANGUAGE_VERSION)
        )

        return info

    def minimumSizeHint(self):
        return QSize(50, 50)

    def sizeHint(self):
        return QSize(380, 380)

    def setRotation(self, angles):
        #print(angles)
        x=int.from_bytes(angles[0:2], byteorder='big', signed=True)
        #print(angles[0:1])
        y=int.from_bytes(angles[2:4], byteorder='big', signed=True)
        z=int.from_bytes(angles[4:6], byteorder='big', signed=True)
        #print('x:{} y:{} z:{}'.format(x, y, z))
        self.setXRotation(x*10+3600)
        self.setYRotation(y*10+3600)
        self.setZRotation(z*10+3600)

        self.xslider.setValue(x)
        self.yslider.setValue(y)
        self.update()

    def setXRotation(self, angle):
        #print('x: {}'.format(angle))
        angle = self.normalizeAngle(angle)
        if angle != self.xRot:
            self.xRot = angle
            self.xRotationChanged.emit(angle)
            #self.update()

    def setYRotation(self, angle):
        #print('y: {}'.format(angle))
        angle = self.normalizeAngle(angle)
        if angle != self.yRot:
            self.yRot = angle
            self.yRotationChanged.emit(angle)
            #self.update()

    def setZRotation(self, angle):
        #print('z: {}'.format(angle))
        angle = self.normalizeAngle(angle)
        if angle != self.zRot:
            self.zRot = angle
            self.zRotationChanged.emit(angle)
            #self.update()

    def initializeGL(self):
        m_UnitsPerPixel = 1
        
        m_PhysicalPosition_x = 0
        m_PhysicalPosition_y = 0
        parentPhysicalPosition_x = 0
        parentPhysicalPosition_y = 0
        m_PhysicalSize_x = 94
        m_PhysicalSize_y = 98
        m_Scale_x = 1.0
        m_Scale_y = 1.0
        
        #have to cast to GLint, GLsizei explicitly with update to PyOpenGL 3.0.0a5
        #stumath.pid, but true
        
        #The location in math.pixels is calculated based on the size of the
        #gauge component and the offset of the parent guage
        m_PixelPosition_x = gl.GLsizei(int((m_PhysicalPosition_x * m_Scale_x + parentPhysicalPosition_x ) / m_UnitsPerPixel))
        m_PixelPosition_y = gl.GLsizei(int((m_PhysicalPosition_y * m_Scale_y + parentPhysicalPosition_y ) / m_UnitsPerPixel))
        
        #The size in pixels of the gauge is the physical size / mm per pixel
        m_PixelSize_x = gl.GLint(int(m_PhysicalSize_x / m_UnitsPerPixel * m_Scale_x))
        m_PixelSize_y = gl.GLint(int(m_PhysicalSize_y / m_UnitsPerPixel * m_Scale_y))
        
        
        #self.SetCurrent()
        gl.glViewport(m_PixelPosition_x, m_PixelPosition_y, m_PixelSize_x, m_PixelSize_y)
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        
        #Define the projection so that we're drawing in "real" space
        gl.glOrtho(0, m_Scale_x * m_PhysicalSize_x, 0, m_Scale_y * m_PhysicalSize_y, -1, 1)
        #Prepare the modelview matrix
        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glLoadIdentity()
        gl.glScalef(m_Scale_x, m_Scale_y, 1.0)

    def paintGL(self):
        
        aCircle = CircleEvaluator()
                
        Roll = self.Roll
        Pitch = self.Pitch
        
        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glPushMatrix()
        
        #Move to the center of the window
        gl.glTranslated(47,49,0)
        #Rotate based on the bank
        gl.glRotated(Roll, 0, 0, 1)
        
        #Translate in the direction of the rotation based
        #on the math.pitch. On the 777, a math.pitch of 1 degree = 2 mm
        gl.glTranslated(0, Pitch * -2.0, 0)
        
        #-------------------Gauge Background------------------
        #It's drawn oversize to allow for math.pitch and bank
        
        #The "ground" rectangle
        #Remember, the coordinate system is now centered in the gauge component
        gl.glColor3ub(179,102,0)
        
        gl.glBegin(gl.GL_POLYGON)
        gl.glVertex2f(-300,-300)
        gl.glVertex2f(-300,0)
        gl.glVertex2f(300,0)
        gl.glVertex2f(300,-300)
        gl.glVertex2f(-300,-300)
        gl.glEnd()
        
        #The "sky" rectangle
        #Remember, the coordinate system is now centered in the gauge component
        gl.glColor3ub(0,153,204)
        
        gl.glBegin(gl.GL_POLYGON)
        gl.glVertex2f(-300,0)
        gl.glVertex2f(-300,300)
        gl.glVertex2f(300,300)
        gl.glVertex2f(300,0)
        gl.glVertex2f(-300,0)
        gl.glEnd()
        
        #------------Draw the math.pitch markings--------------
        
        #Draw in white
        gl.glColor3ub(255,255,255)
        #Specify line width
        gl.glLineWidth(1.0)
        #The size for all math.pitch text
        #m_pFontManager->SetSize(m_Font,4.0, 4.0)
        
        gl.glBegin(gl.GL_LINES)
        
        #The dividing line between sky and ground
        gl.glVertex2f(-100,0)
        gl.glVertex2f(100,0)
        
        # +2.5 degrees
        gl.glVertex2f(-5,5)
        gl.glVertex2f(5,5)
        
        # +5.0 degrees
        gl.glVertex2f(-10,10)
        gl.glVertex2f(10,10)
        
        # +7.5 degrees
        gl.glVertex2f(-5,15)
        gl.glVertex2f(5,15)
        
        # +10.0 degrees
        gl.glVertex2f(-20,20)
        gl.glVertex2f(20,20)
        
        # +12.5 degrees
        gl.glVertex2f(-5,25)
        gl.glVertex2f(5,25)
        
        # +15.0 degrees
        gl.glVertex2f(-10,30)
        gl.glVertex2f(10,30)
        
        # +17.5 degrees
        gl.glVertex2f(-5,35)
        gl.glVertex2f(5,35)
        
        # +20.0 degrees
        gl.glVertex2f(-20,40)
        gl.glVertex2f(20,40)
        
        # -2.5 degrees
        gl.glVertex2f(-5,-5)
        gl.glVertex2f(5,-5)
        
        # -5.0 degrees
        gl.glVertex2f(-10,-10)
        gl.glVertex2f(10,-10)
        
        # -7.5 degrees
        gl.glVertex2f(-5,-15)
        gl.glVertex2f(5,-15)
        
        # -10.0 degrees
        gl.glVertex2f(-20,-20)
        gl.glVertex2f(20,-20)
        
        # -12.5 degrees
        gl.glVertex2f(-5,-25)
        gl.glVertex2f(5,-25)
        
        # -15.0 degrees
        gl.glVertex2f(-10,-30)
        gl.glVertex2f(10,-30)
        
        # -17.5 degrees
        gl.glVertex2f(-5,-35)
        gl.glVertex2f(5,-35)
        
        # -20.0 degrees
        gl.glVertex2f(-20,-40)
        gl.glVertex2f(20,-40)
        
        gl.glEnd()
        
        #### +10
        ###m_pFontManager->Print(-27.5,18.0,"10",m_Font)
        ###m_pFontManager->Print(21.0,18.0,"10",m_Font)
        ###
        #### -10
        ###m_pFontManager->Print(-27.5,-22.0,"10",m_Font)
        ###m_pFontManager->Print(21.0,-22.0,"10",m_Font)
        ###
        #### +20
        ###m_pFontManager->Print(-27.5,38.0,"20",m_Font)
        ###m_pFontManager->Print(21.0,38.0,"20",m_Font)
        ###
        #### -20
        ###m_pFontManager->Print(-27.5,-42.0,"20",m_Font)
        ###m_pFontManager->Print(21.0,-42.0,"20",m_Font)
        
        #-----The background behind the bank angle markings-------
        # Reset the modelview matrix
        gl.glPopMatrix()
        gl.glPushMatrix()
        
        # Draw in the sky color
        gl.glColor3ub(0,153,204)
        
        aCircle.SetOrigin(47,49)
        aCircle.SetRadius(46)
        aCircle.SetDegreesPerPoint(5)
        aCircle.SetArcStartEnd(300.0,360.0)
        
        gl.glBegin(gl.GL_TRIANGLE_FAN)
        gl.glVertex2f(0,98)
        gl.glVertex2f(0,72)
        aCircle.Evaluate()
        gl.glVertex2f(47,98)
        gl.glEnd()
        
        aCircle.SetArcStartEnd(0.0,60.0)
        gl.glBegin(gl.GL_TRIANGLE_FAN)
        gl.glVertex2f(94,98)
        gl.glVertex2f(47,98)
        aCircle.Evaluate()
        gl.glVertex2f(94,72)
        gl.glEnd()
        
        #----------------The bank angle markings----------------
        
        # Left side bank markings
        # Reset the modelview matrix
        gl.glPopMatrix()
        gl.glPushMatrix()
        
        # Draw in white
        gl.glColor3ub(255,255,255)
        
        # Move to the center of the window
        gl.glTranslated(47,49,0)
        
        # Draw the center detent
        gl.glBegin(gl.GL_POLYGON)
        gl.glVertex2f(0.0,46.0)
        gl.glVertex2f(-2.3,49.0)
        gl.glVertex2f(2.3,49.0)
        gl.glVertex2f(0.0,46.0)
        gl.glEnd()
        
        gl.glRotated(10.0,0,0,1)
        gl.glBegin(gl.GL_LINES)
        gl.glVertex2f(0,46)
        gl.glVertex2f(0,49)
        gl.glEnd()
        
        gl.glRotated(10.0,0,0,1)
        gl.glBegin(gl.GL_LINES)
        gl.glVertex2f(0,46)
        gl.glVertex2f(0,49)
        gl.glEnd()
        
        gl.glRotated(10.0,0,0,1)
        gl.glBegin(gl.GL_LINES)
        gl.glVertex2f(0,46)
        gl.glVertex2f(0,53)
        gl.glEnd()
        
        gl.glRotated(15.0,0,0,1)
        gl.glBegin(gl.GL_LINES)
        gl.glVertex2f(0,46)
        gl.glVertex2f(0,49)
        gl.glEnd()
        
        gl.glRotated(15.0,0,0,1)
        gl.glBegin(gl.GL_LINES)
        gl.glVertex2f(0,46)
        gl.glVertex2f(0,51)
        gl.glEnd()
        
        # Right side bank markings
        # Reset the modelview matrix
        gl.glPopMatrix()
        gl.glPushMatrix()
        # Move to the center of the window
        gl.glTranslated(47,49,0)
        
        gl.glRotated(-10.0,0,0,1)
        gl.glBegin(gl.GL_LINES)
        gl.glVertex2f(0,46)
        gl.glVertex2f(0,49)
        gl.glEnd()
        
        gl.glRotated(-10.0,0,0,1)
        gl.glBegin(gl.GL_LINES)
        gl.glVertex2f(0,46)
        gl.glVertex2f(0,49)
        gl.glEnd()
        
        gl.glRotated(-10.0,0,0,1)
        gl.glBegin(gl.GL_LINES)
        gl.glVertex2f(0,46)
        gl.glVertex2f(0,53)
        gl.glEnd()
        
        gl.glRotated(-15.0,0,0,1)
        gl.glBegin(gl.GL_LINES)
        gl.glVertex2f(0,46)
        gl.glVertex2f(0,49)
        gl.glEnd()
        
        gl.glRotated(-15.0,0,0,1)
        gl.glBegin(gl.GL_LINES)
        gl.glVertex2f(0,46)
        gl.glVertex2f(0,51)
        gl.glEnd()
        
        #----------------End Draw Bank Markings----------------
        
        
        #----------------Bank Indicator----------------
        # Reset the modelview matrix
        gl.glPopMatrix()
        gl.glPushMatrix()
        # Move to the center of the window
        gl.glTranslated(47,49,0)
        # Rotate based on the bank
        gl.glRotated(Roll, 0, 0, 1)
        
        # Draw in white
        gl.glColor3ub(255,255,255)
        # Specify line width
        gl.glLineWidth(2.0)
        
        gl.glBegin(gl.GL_LINE_LOOP) # the bottom rectangle
        gl.glVertex2f(-4.5, 39.5)
        gl.glVertex2f(4.5, 39.5)
        gl.glVertex2f(4.5, 41.5)
        gl.glVertex2f(-4.5, 41.5)
        gl.glEnd()
        
        gl.glBegin(gl.GL_LINE_STRIP) # the top triangle
        gl.glVertex2f(-4.5, 41.5)
        gl.glVertex2f(0, 46)
        gl.glVertex2f(4.5, 41.5)
        gl.glEnd()
        
        #--------------End draw bank indicator------------
        
        #----------------Attitude Indicator----------------
        # Reset the modelview matrix
        gl.glPopMatrix()
        gl.glPushMatrix()
        # Move to the center of the window
        gl.glTranslated(47,49,0)
        
        # The center axis indicator
        # Black background
        gl.glColor3ub(0,0,0)
        gl.glBegin(gl.GL_POLYGON)
        gl.glVertex2f(1.25,1.25)
        gl.glVertex2f(1.25,-1.25)
        gl.glVertex2f(-1.25,-1.25)
        gl.glVertex2f(-1.25,1.25)
        gl.glVertex2f(1.25,1.25)
        gl.glEnd()
        # White lines
        gl.glColor3ub(255,255,255)
        gl.glLineWidth(2.0)
        gl.glBegin(gl.GL_LINE_LOOP)
        gl.glVertex2f(1.25,1.25)
        gl.glVertex2f(1.25,-1.25)
        gl.glVertex2f(-1.25,-1.25)
        gl.glVertex2f(-1.25,1.25)
        gl.glEnd()
        
        # The left part
        # Black background
        gl.glColor3ub(0,0,0)
        gl.glBegin(gl.GL_POLYGON)
        gl.glVertex2f(-39,1.25)
        gl.glVertex2f(-19,1.25)
        gl.glVertex2f(-19,-1.25)
        gl.glVertex2f(-39,-1.25)
        gl.glVertex2f(-39,1.25)
        gl.glEnd()
        gl.glBegin(gl.GL_POLYGON)
        gl.glVertex2f(-19,1.25)
        gl.glVertex2f(-19,-5.75)
        gl.glVertex2f(-22,-5.75)
        gl.glVertex2f(-22,1.25)
        gl.glVertex2f(-19,1.25)
        gl.glEnd()
        
        # White lines
        gl.glColor3ub(255,255,255)
        gl.glLineWidth(2.0)
        gl.glBegin(gl.GL_LINE_LOOP)
        gl.glVertex2f(-39,1.25)
        gl.glVertex2f(-19,1.25)
        gl.glVertex2f(-19,-5.75)
        gl.glVertex2f(-22,-5.75)
        gl.glVertex2f(-22,-1.25)
        gl.glVertex2f(-39,-1.25)
        gl.glEnd()
        
        # The right part
        # Black background
        gl.glColor3ub(0,0,0)
        gl.glBegin(gl.GL_POLYGON)
        gl.glVertex2f(39,1.25)
        gl.glVertex2f(19,1.25)
        gl.glVertex2f(19,-1.25)
        gl.glVertex2f(39,-1.25)
        gl.glVertex2f(39,1.25)
        gl.glEnd()
        gl.glBegin(gl.GL_POLYGON)
        gl.glVertex2f(19,1.25)
        gl.glVertex2f(19,-5.75)
        gl.glVertex2f(22,-5.75)
        gl.glVertex2f(22,1.25)
        gl.glVertex2f(19,1.25)
        gl.glEnd()
        
        # White lines
        gl.glColor3ub(255,255,255)
        gl.glLineWidth(2.0)
        gl.glBegin(gl.GL_LINE_LOOP)
        gl.glVertex2f(39,1.25)
        gl.glVertex2f(19,1.25)
        gl.glVertex2f(19,-5.75)
        gl.glVertex2f(22,-5.75)
        gl.glVertex2f(22,-1.25)
        gl.glVertex2f(39,-1.25)
        gl.glEnd()
        #--------------End draw attitude indicator------------
        # Reset the modelview matrix
        gl.glPopMatrix()
        gl.glPushMatrix()
        
        aCircle.SetRadius(3.77)
        #-------------------Rounded corners------------------
        # The corners of the artificial horizon are rounded off by
        # drawing over them in black. The overlays are essentially the
        # remainder of a circle subtracted from a square, and are formed
        # by fanning out triangles from a point just off each corner
        # to an arc descrbing the curved portion of the art. horiz.
        
#        glColor3ub(0,0,0)
#        # Lower left
#        glBegin(GL_TRIANGLE_FAN)
#        glVertex2f(-1.0,-1.0)
#        aCircle.SetOrigin(3.77,3.77)
#        aCircle.SetArcStartEnd(180,270)
#        aCircle.SetDegreesPerPoint(15)
#        aCircle.Evaluate()
#        glEnd()
#        # Upper left
#        glBegin(GL_TRIANGLE_FAN)
#        glVertex2f(-1.0,99.0)
#        aCircle.SetOrigin(3.77,94.23)
#        aCircle.SetArcStartEnd(270,360)
#        aCircle.SetDegreesPerPoint(15)
#        aCircle.Evaluate()
#        glEnd()
#        # Upper right
#        glBegin(GL_TRIANGLE_FAN)
#        glVertex2f(95.0,99.0)
#        aCircle.SetOrigin(90.23,94.23)
#        aCircle.SetArcStartEnd(0,90)
#        aCircle.SetDegreesPerPoint(15)
#        aCircle.Evaluate()
#        glEnd()
#        #Lower right
#        glBegin(GL_TRIANGLE_FAN)
#        glVertex2f(95.0,-1)
#        aCircle.SetOrigin(90.23,3.77)
#        aCircle.SetArcStartEnd(90,180)
#        aCircle.SetDegreesPerPoint(15)
#        aCircle.Evaluate()
#        glEnd()
        
        # Finally, restore the modelview matrix to what we received
        gl.glPopMatrix()
        
        gl.glFlush()
        #self.SwapBuffers()
    #end of method draw
        

    def resizeGL(self, width, height):
        side = min(width, height)
        if side < 0:
            return

        gl.glViewport((width - side) // 2, (height - side) // 2, side,
                           side)

        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        gl.glOrtho(-0.5, +0.5, +0.5, -0.5, 4.0, 15.0)
        gl.glMatrixMode(gl.GL_MODELVIEW)

    def mousePressEvent(self, event):
        self.lastPos = event.pos()

    def mouseMoveEvent(self, event):
        dx = event.x() - self.lastPos.x()
        dy = event.y() - self.lastPos.y()

        if event.buttons() & Qt.LeftButton:
            self.setXRotation(self.xRot + 8 * dy)
            self.setYRotation(self.yRot + 8 * dx)
        elif event.buttons() & Qt.RightButton:
            self.setXRotation(self.xRot + 8 * dy)
            self.setZRotation(self.zRot + 8 * dx)

        self.lastPos = event.pos()

    def setClearColor(self, c):
        gl.glClearColor(c.redF(), c.greenF(), c.blueF(), c.alphaF())

    def setColor(self, c):
        gl.glColor4f(c.redF(), c.greenF(), c.blueF(), c.alphaF())
