from PyQt5 import QtGui, QtCore, QtWidgets, QtOpenGL
from ui_calibration import Ui_caliBration

class Calibration_GUI(QtWidgets.QWidget):
    def __init__(self, radio):
        QtWidgets.QWidget.__init__(self, None)
        self.ui = Ui_caliBration()
        self.ui.setupUi(self)
        self.ui.motorSet.clicked.connect(self.motorSet)
        self.ui.radioMotor0.clicked.connect(self.radioMotor0)
        self.ui.radioMotor1.clicked.connect(self.radioMotor1)
        self.ui.radioMotor2.clicked.connect(self.radioMotor2)
        self.ui.radioMotor3.clicked.connect(self.radioMotor3)
        self.ui.motorOffset.valueChanged.connect(self.motorOffset)
        self.radio = radio
        self.motor = 0

    def motorSet(self):
        
        pass

    def radioMotor0(self):
        self.motor = 0
        
    def radioMotor1(self):
        self.motor = 1

    def radioMotor2(self):
        self.motor = 2

    def radioMotor3(self):
        self.motor = 3

    def motorOffset(self):
        print('motoroffset change')
