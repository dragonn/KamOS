from PyQt5 import QtGui, QtCore, QtWidgets, QtOpenGL
from ui_nav import Ui_Nav
import time
import math

class Nav_GUI(QtWidgets.QWidget):
    def __init__(self, radio, main):
        QtWidgets.QWidget.__init__(self, None)
        self.ui = Ui_Nav()
        self.ui.setupUi(self)

        self.navScene = QtWidgets.QGraphicsScene(self)   
        self.mapImage = QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("./images/map.png"))
        self.navScene.addItem(self.mapImage)
        self.ui.navView.setScene(self.navScene)

        self.start = [537-5, 529-5]

        self.mapPoint = QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("./images/mappoint.png"))
        self.mapPoint.setZValue(1)
        self.navScene.addItem(self.mapPoint)

        self.mapPoint.setOffset(self.start[0], self.start[1])

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.powernav)
        self.timer.start(1000/30)
        self.main = main

        self.time = time.time()

        self.speed = 0.75*1000*2 #m/s

        self.x = self.start[0]
        self.y = self.start[1]

        self.timer2 = QtCore.QTimer()
        self.timer2.start(1000)
        self.timer2.timeout.connect(self.powerfix)

        self.ui.navView.mousePressEvent=self.setxy
        
    def setxy(self, event):
        #print('click')
        #print(event)
        #print(event.x())
        #print(event.y())
        x=event.x()-5
        y=event.y()-5
        self.mapPoint.setOffset(x, y)
        self.x=x
        self.y=y
        pass

    def update(self, values):
        x=int.from_bytes(values[0:4], byteorder='big', signed=True)
        y=int.from_bytes(values[4:8], byteorder='big', signed=True)

        #40 pixeli - 2m

        x=x*0.02
        y=y*0.02

        x+=self.start[0]
        y+=self.start[1]

        self.mapPoint.setOffset(x, y)
        pass

    def updatexy(self, x, y):
        x=x*0.02
        y=y*0.02

        #x+=self.start[0]
        #y+=self.start[1]


        self.x+=x
        self.y+=y

        #print(self.x)
        self.mapPoint.setOffset(self.x, self.y)
        pass

    def powernav(self):
        
        distance = (self.speed * self.main.power) * (time.time() - self.time)
        angle = self.main.acl.yRot
        self.time = time.time()
        #print(distance/1000)

        x=math.cos(math.radians(angle))*distance
        y=math.sin(math.radians(angle))*distance

        #print(x)
        #print(y)
        self.updatexy(x, y)
        #print()


        #print(self.main.power)
        #print(time.time())
        #print('powerupdate')

    def powerfix(self):
        print('powerfix')
        mapPoint = QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("./images/mappointfixed.png"))
        self.navScene.addItem(mapPoint)

        mapPoint.setOffset(self.x, self.y)


