import time
import socket
import config
import cobs

from PyQt5 import QtCore

import qradio_base

BUFFER_SIZE = 1024

class QRadio(qradio_base.QRadio):
    def __init__(self):
        qradio_base.QRadio.__init__(self)

    def open(self):
        connected = False
        while not connected:
            try:
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.socket.connect((config.radio['ip'], config.radio['port']))
                self.socket.setblocking(False)
                self.buffer=b''
                connected = True
            except Exception as e:
                print(e)
                print("open_tcp error")
                time.sleep(1)

    def read(self):
        try:
            return self.socket.recv(1024)
        except BlockingIOError as e:
            return b''

    def write(self, message):
        self.socket.send(message)
        