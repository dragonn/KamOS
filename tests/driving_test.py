import serial
import time
import crcmod.predefined
from cobs import cobs
crc16 = crcmod.predefined.mkCrcFun('modbus')
ser = serial.Serial(port='/dev/ttyUSB0', baudrate=115200)
time.sleep(1)

def hexprint(value):
     print(''.join('0x{:02x} '.format(x) for x in value))

message = b'\x01\x00\x00\x2D\x2D\x00\x00'
crc_calc=crc16(message).to_bytes(2, byteorder='big')
print(crc_calc)
message+=crc16(message).to_bytes(2, byteorder='big')

message = cobs.encode(message)+b'\x00'
print(message)
print(len(message))
#ser.write(message)

#433
#ser.write(b"433.5\n")
#ser.write(b"20\n")
#ser.write(bytes([0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02]))

#868
#ser.write(b"868.5\n")
#ser.write(b"20\n")
#ser.write(bytes([0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02]))
#exit()
out=b''
time.sleep(0.2)
while ser.inWaiting() > 0:
    out += ser.read(1)
print(out)


angle1=0
angle2=0
power=0
message = b'\x01\x00\x00'+angle1.to_bytes(1, byteorder='big')+angle2.to_bytes(1, byteorder='big')+power.to_bytes(1, byteorder='big')+b'\x01'
message+=crc16(message).to_bytes(2, byteorder='big')
message = cobs.encode(message)+b'\x00'
time.sleep(1)

for i in range(0, 91):
    angle1=i
    angle2=i
    power=0
    message = b'\x01\x00\x00'+angle1.to_bytes(1, byteorder='big')+angle2.to_bytes(1, byteorder='big')+power.to_bytes(1, byteorder='big')+b'\x01'
    message+=crc16(message).to_bytes(2, byteorder='big')
    message = cobs.encode(message)+b'\x00'
    ser.write(message)
    time.sleep(0.01)


print(angle1)
print(angle2)
time.sleep(1)
while ser.inWaiting() > 0:
    out += ser.read(1)
print(''.join('0x{:02x} '.format(x) for x in out))
print(out[-1])
print(out[:-1])
out2=cobs.decode(out[:-1])
hexprint(out2)
crc=out2[-2:]
hexprint(crc)
hexprint(crc16(out2[:-2]).to_bytes(2, byteorder='big'))
