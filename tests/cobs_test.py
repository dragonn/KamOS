from cobs import cobs

encoded=cobs.encode(b'\x02\x01\x00\x22\x44\xbf\x54')
print(''.join('0x{:02x} '.format(x) for x in encoded))
