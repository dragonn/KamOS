import serial
import time
import crcmod.predefined
from cobs import cobs
from sys import stdin
import sys

crc16 = crcmod.predefined.mkCrcFun('modbus')
ser = serial.Serial(port='/dev/ttyUSB0', baudrate=115200)
time.sleep(1)

print("Numer silnika [1-4]: ", end='')
sys.stdout.flush()
userinput = stdin.readline()
motor=int(userinput)


while True:
    
    
    print("Kąt kalibracji [-90 do 90]: ", end='')
    sys.stdout.flush()
    userinput = stdin.readline()
    angle=int(userinput)
    
    if angle < 0:
        angle*=-1
        direction=1
    else:
        direction=2
    
    motoranddirection=motor*10+direction
    
    message = b'\x01\x00\x01'+motoranddirection.to_bytes(1, byteorder='big')+angle.to_bytes(1, byteorder='big')
    message+=crc16(message).to_bytes(2, byteorder='big')
    message=cobs.encode(message)+b'\x00'
    ser.write(message)
    
    for i in range(0, 10):
        message = b'\x01\x00\x00\x5A\x5A\x00\x00'
        crc_calc=crc16(message).to_bytes(2, byteorder='big')
        message+=crc16(message).to_bytes(2, byteorder='big')
        message = cobs.encode(message)+b'\x00'
        ser.write(message)
        time.sleep(0.2)
