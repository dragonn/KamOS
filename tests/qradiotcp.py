import socket
import sys
from cobs import cobs
HOST = ''   # Symbolic name, meaning all available interfaces
PORT = 3307 # Arbitrary non-privileged port
 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print('Socket created')
 
#Bind socket to local host and port
try:
    s.bind((HOST, PORT))
except socket.error as msg:
    print('Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
    sys.exit()
     
print('Socket bind complete')
 
#Start listening on socket
s.listen(10)
print('Socket now listening')
 
#now keep talking with the client
while 1:
    #wait to accept a connection - blocking call
    conn, addr = s.accept()
    print('Connected with ' + addr[0] + ':' + str(addr[1]))
    try:
        data = b''
        while True:
            data += conn.recv(1024)
            if data:
                #print(data)
                #print(data[-1])
                index=data.find(b'\x00')
                #print(index)
                if index > -1:
                    decode = cobs.decode(data[:index])
                    data = data[index+1:]
                    print(''.join('{:02x} '.format(x) for x in decode))
            else:
                break
            conn.send(cobs.encode(b'\x02\x01\x55\x22\x44\xbf\x54')+b'\x00')
    finally:
        conn.close()
     
s.close()