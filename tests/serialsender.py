from cobs import cobs
import crcmod
import serial

crc16 = crcmod.predefined.mkCrcFun('modbus')

port = input("Podaj port: ") 
speed = int(input("Podaj szybkosć: "))

com = serial.Serial(port, speed, timeout=1)

while True:
    packet = bytes.fromhex(input("Podaj pakiet w hex: "))
    packet += crc16(packet).to_bytes(2, byteorder='big')
    packet = cobs.encode(packet)+b'\x00'
    com.write(packet)
    
    packet = com.read(1024)
    print('Odpowiedz: '+''.join('{:02x} '.format(x) for x in packet))
