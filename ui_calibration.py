# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_calibration.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_caliBration(object):
    def setupUi(self, caliBration):
        caliBration.setObjectName("caliBration")
        caliBration.resize(246, 103)
        self.radioMotor0 = QtWidgets.QRadioButton(caliBration)
        self.radioMotor0.setGeometry(QtCore.QRect(95, 0, 41, 23))
        self.radioMotor0.setObjectName("radioMotor0")
        self.radioMotor1 = QtWidgets.QRadioButton(caliBration)
        self.radioMotor1.setGeometry(QtCore.QRect(133, 0, 41, 23))
        self.radioMotor1.setObjectName("radioMotor1")
        self.radioMotor2 = QtWidgets.QRadioButton(caliBration)
        self.radioMotor2.setGeometry(QtCore.QRect(172, 0, 41, 23))
        self.radioMotor2.setObjectName("radioMotor2")
        self.radioMotor3 = QtWidgets.QRadioButton(caliBration)
        self.radioMotor3.setGeometry(QtCore.QRect(210, 0, 41, 23))
        self.radioMotor3.setObjectName("radioMotor3")
        self.label = QtWidgets.QLabel(caliBration)
        self.label.setGeometry(QtCore.QRect(6, 3, 91, 18))
        self.label.setObjectName("label")
        self.motorOffset = QtWidgets.QSlider(caliBration)
        self.motorOffset.setGeometry(QtCore.QRect(0, 30, 241, 21))
        self.motorOffset.setMinimum(-90)
        self.motorOffset.setMaximum(90)
        self.motorOffset.setOrientation(QtCore.Qt.Horizontal)
        self.motorOffset.setObjectName("motorOffset")
        self.motorSet = QtWidgets.QPushButton(caliBration)
        self.motorSet.setGeometry(QtCore.QRect(74, 60, 92, 30))
        self.motorSet.setObjectName("motorSet")
        self.currentValue = QtWidgets.QLabel(caliBration)
        self.currentValue.setGeometry(QtCore.QRect(220, 66, 21, 18))
        self.currentValue.setObjectName("currentValue")

        self.retranslateUi(caliBration)
        QtCore.QMetaObject.connectSlotsByName(caliBration)

    def retranslateUi(self, caliBration):
        _translate = QtCore.QCoreApplication.translate
        caliBration.setWindowTitle(_translate("caliBration", "Form"))
        self.radioMotor0.setText(_translate("caliBration", "0"))
        self.radioMotor1.setText(_translate("caliBration", "1"))
        self.radioMotor2.setText(_translate("caliBration", "2"))
        self.radioMotor3.setText(_translate("caliBration", "3"))
        self.label.setText(_translate("caliBration", "Numer silnika"))
        self.motorSet.setText(_translate("caliBration", "Ustaw"))
        self.currentValue.setText(_translate("caliBration", "0"))

