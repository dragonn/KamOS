class Qrelay():
    def __init__(self, qradio, ui):
        self.ui=ui
        self.qradio=qradio

        self.ui.relayOn1.clicked.connect(self.relay1on)
        self.ui.relayOff1.clicked.connect(self.relay1off)

        self.ui.relayOn2.clicked.connect(self.relay2on)
        self.ui.relayOff2.clicked.connect(self.relay2off)

        self.ui.relayOn3.clicked.connect(self.relay3on)
        self.ui.relayOff3.clicked.connect(self.relay3off)

    
    def relay1on(self):
        self.qradio.put_confrim(0x02, 10, [1, 1])

    def relay1off(self):
        self.qradio.put_confrim(0x02, 10, [1, 0])


    def relay2on(self):
        self.qradio.put_confrim(0x02, 10, [2, 1])

    def relay2off(self):
        self.qradio.put_confrim(0x02, 10, [2, 0])


    def relay3on(self):
        self.qradio.put_confrim(0x02, 10, [3, 1])

    def relay3off(self):
        self.qradio.put_confrim(0x02, 10, [3, 0])